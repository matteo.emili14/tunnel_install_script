#! /bin/bash

#ottengo ipv4 del server
ipv4=$(ip -4 addr show eth0 | grep -oP '(?<=inet\s)\d+(\.\d+){3}')

echo "Inserisci indirizzo IPV6 assegnato al server"
read ipv6
echo "Inserisci gateway IPV6"
read ipv6_gateway
echo "Inserisci routed subnet IPV6 senza prefisso e con :: finali"
read routed_subnet

#aggiungo indirizzo ipv6 al server
echo "iface eth0 inet6 static" >> /etc/network/interfaces
echo "  address $ipv6" >> /etc/network/interfaces
echo "  netmask 48" >> /etc/network/interfaces
echo "  gateway $ipv6_gateway" >> /etc/network/interfaces

#installazione dipendenze necessarie
apt update
apt install -y sipcalc unzip

#download e configurazione dello script per il tunnel
wget https://github.com/sskaje/6in4/archive/refs/heads/master.zip
unzip master.zip
rm ~/6in4-master/etc/config.ini
touch ~/6in4-master/etc/config.ini
echo "IPV6_NETWORK=$routed_subnet" >> /home/6in4-master/etc/config.ini
echo "IPV6_CIDR=48" >> /home/6in4-master/etc/config.ini
echo "INTERFACE=eth0" >> /home/6in4-master/etc/config.ini
echo "BIND_IP=$ipv4" >> /home/6in4-master/etc/config.ini

echo "net.ipv4.ip_forward=1" >> /etc/sysctl.conf
echo "net.ipv6.conf.all.forwarding=1" >> /etc/sysctl.conf
sysctl -p
service networking restart

echo "Per avviare il tunnel usa il comando ./6in4-master/bin/6to4 add 1 ipv4_del_client"